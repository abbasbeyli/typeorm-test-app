import { MiddlewareInterface, NextFn, ResolverData } from "type-graphql";
import { Inject } from "typedi";
import { Context } from "vm";
import { Logger } from "../services/Logger";

export class LoggerMiddleware implements MiddlewareInterface<Context> {
  constructor(@Inject() private readonly logger: Logger) { }

  use({ info }: ResolverData, next: NextFn) {
    // extract `extensions` object from GraphQLResolveInfo object to get the `logMessage` value
    const { logMessage } = info.parentType.getFields()[info.fieldName].extensions || {};

    if (logMessage) {
      this.logger.log(logMessage);
    }

    return next();
  }
}