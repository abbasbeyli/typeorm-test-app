import { Field, ID, ObjectType } from "type-graphql";
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, ManyToOne, ManyToMany } from "typeorm";
import { Album } from "./Album";
import { Author } from "./Author";
import { PhotoMetadata } from "./PhotoMetadata";

@ObjectType()
@Entity()
export class Photo {

    @Field(type => ID)
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column({ length: 100 })
    name: string;

    @Field({ nullable: true })
    @Column("text")
    description: string;

    @Field()
    @Column()
    filename: string;

    @Field()
    @Column("double")
    views: number;

    @Field()
    @Column()
    isPublished: boolean;

    @Field({ simple: true, nullable: true })
    @OneToOne(type => PhotoMetadata, photoMetadata => photoMetadata.photo, { cascade: true  })
    metadata: PhotoMetadata;

    @Field({ simple: false, nullable: true })
    @ManyToOne(type => Author, author => author.photos, { cascade: true  })
    author: Author;

    @ManyToMany(type => Album, album => album.photos)
    albums: Album[];
}