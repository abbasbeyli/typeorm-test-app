import { Field, ID, ObjectType } from "type-graphql";
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from "typeorm";
import { Photo } from "./Photo";

@ObjectType()
@Entity()
export class PhotoMetadata {

    @Field(type => ID)
    @PrimaryGeneratedColumn()
    id: number;

    @Field({ nullable: true })
    @Column("int")
    height: number;

    @Field({ nullable: true })
    @Column("int")
    width: number;

    @Field({ nullable: true })
    @Column()
    orientation: string;

    @Field({ nullable: true })
    @Column()
    compressed: boolean;

    @Field({ nullable: true })
    @Column()
    comment: string;

    @OneToOne(type => Photo, photo => photo.metadata)
    @JoinColumn()
    photo: Photo;
}