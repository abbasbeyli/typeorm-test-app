import { IsEmail, Length } from "class-validator";
import { Field, ID, ObjectType } from "type-graphql";
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
@ObjectType()
export class User {

    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column()
    firstName: string;

    @Field()
    @Column()
    lastName: string;

    @Length(1, 37)
    @IsEmail()
    @Field()
    @Column({ unique: true })
    email: string;

    @Field()
    @Column()
    password:string

}