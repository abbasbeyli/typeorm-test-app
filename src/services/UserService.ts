import { Inject, Service } from "typedi";
import bcrypt from "bcryptjs";

import { User } from "../entity";
import { UserInput } from "../module/models/UserInput";
import { BaseEntityService } from "./BaseEntityService";
import { MapperSevice } from "./MapperService";

@Service()
export class UserService extends BaseEntityService<User, number, UserInput> {

    @Inject()
    mapperSevice: MapperSevice;

    entity = User;


    override async validateAndMapForSave(data: UserInput, user: User): Promise<User> {
        console.log('validateAndMapForSave', { data, user });

        const item = this.mapperSevice.map(data, new User());
        item.password = await bcrypt.hash(item.password, 12);

        return item;
    }

}