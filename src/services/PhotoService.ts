import { Inject, Service } from "typedi";

import { Author, Photo, PhotoMetadata, User } from "../entity";
import { PhotoInput } from "../module/models/PhotoInput";
import { PhotoMetadataInput } from "../module/models/PhotoMetadataInput";
import { BaseEntityService } from "./BaseEntityService";
import { MapperSevice } from "./MapperService";

@Service()
export class PhotoService extends BaseEntityService<Photo, number, PhotoInput> {

    @Inject()
    mapperSevice: MapperSevice;

    entity = Photo;


    override async validateAndMapForSave(data: PhotoInput, user: User): Promise<Photo> {
        console.log('validateAndMapForSave', { data, user });

        const item = this.mapperSevice.map(data, new Photo());
        
        if (data.metadata)
            item.metadata = this.mapperSevice.map(data.metadata, new PhotoMetadata());

        if (data.author)
            item.author = this.mapperSevice.map(data.author, new Author());

        return item;
    }

    override async removeByIdAsync(id: number, transaction: boolean = true) {
        const repository = await this.getRepositoryAsync();
        const metadataRepository = (await this.connectionService.getConnection()).getRepository<PhotoMetadata>(PhotoMetadata);
        const result = await repository.findOne(id, {
            join: {
                alias: "photo",
                leftJoinAndSelect: { "metadata": "photo.metadata", "author": "photo.author" },
            } /*relations: ["metadata", "author"]*/
        });

        if (!result) {
            throw new Error("item_not_found");
        }

        // if (result.metadata) {
        //     await metadataRepository.remove(result.metadata, { transaction });
        // }

        await repository.remove(result, { transaction });
    }

}