import { Inject } from "typedi";
import { EntityTarget, FindConditions, FindManyOptions, FindOneOptions } from "typeorm";
import { User } from "../entity";
import { ConnectionService } from "./ConnectionService"

export abstract class BaseEntityService<TEntity, TKey, TInputDTO> {

    @Inject()
    protected connectionService: ConnectionService;

    protected abstract entity: EntityTarget<TEntity>;
    abstract validateAndMapForSave(data: TInputDTO, user: User): Promise<TEntity>;

    protected async getRepositoryAsync() {
        const connection = await this.connectionService.getConnection();
        const repository = connection.getRepository(this.entity);

        return repository;
    }

    async findByIdAsync(id: TKey, options: FindOneOptions<TEntity> | undefined = undefined) {
        const repository = await this.getRepositoryAsync();
        const result = await repository.findOne(id, options);

        return result;
    }

    async findAllAsync(options: FindManyOptions<TEntity>) {
        const repository = await this.getRepositoryAsync();
        const result = await repository.find(options);

        return result;
    }

    async removeByIdAsync(id: TKey, transaction: boolean = true) {
        const repository = await this.getRepositoryAsync();
        const result = await repository.findOne(id);

        if (!result) {
            throw new Error("item_not_found");
        }

        await repository.remove(result, { transaction })
    }

    async addAsync(data: TEntity) {
        const repository = await this.getRepositoryAsync();
        const result = await repository.save(data);

        return result;
    }

    async updateAsync(data: TEntity) {
        const repository = await this.getRepositoryAsync();
        const result = await repository.update("", data);

        return result;
    }
}