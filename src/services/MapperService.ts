import { Service } from "typedi";


@Service()
export class MapperSevice {

    public map<TInput, TResult>(input: TInput, result: TResult, exceptions: Record<string, string>[] = []) {
        if (!result || typeof (result) !== 'object') {
            return result;
        }
        const _result: any = result;

        Object
            .entries(input)
            .forEach(([key, value]) => {

                if (value && typeof (value) === 'object') {
                    _result[key] = this.map(value, {});
                }
                else { //if (typeof (value) == typeof (_result[key])) {
                    _result[key] = value;
                }
            });

        return result;
    }
}