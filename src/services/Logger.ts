import { Service } from "typedi";


@Service()
export class Logger {
    log(...messages: string[]) {
        console.log(...messages)
    }
}