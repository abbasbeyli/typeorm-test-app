import { Service } from "typedi";
import { Connection, createConnection } from "typeorm";

@Service()
export class ConnectionService {
    private connection: Connection;

    async getConnection() {
        if (!this.connection || !this.connection.isConnected) {
            this.connection = await createConnection();
        }

        return this.connection;
    }
}