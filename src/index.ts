import "reflect-metadata";
import express from "express";
import { Container } from "typedi";
import { buildSchema } from "type-graphql";
import { ApolloServer } from 'apollo-server-express';
import { BaseContext } from "apollo-server-types";
import { GraphQLRequestContext } from "apollo-server-core";
import jwt from 'express-jwt';

import { PhotoResolver } from "./module/resolvers/PhotoResolver";
import { customAuthChecker } from "./module/auth/customAuthChecker";
import { UserResolver } from "./module/resolvers/UserResolver";

const app = express();
const PORT = process.env.PORT || 4000;
const path = "/graphql";

export const TYPES = {
    Logger: Symbol.for("Logger"),
    BaseService: Symbol.for("BaseService"),
    SettingsService: Symbol.for("SettingsService"),
};


const main = async () => {

    const schema = await buildSchema({
        resolvers: [PhotoResolver, UserResolver],
        container: Container,
        authChecker: customAuthChecker,
        authMode: "null",
    });

    const server = new ApolloServer({
        schema,
        context: ({ req }) => {
            const requestId = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER).toString();
            const container = Container.of(requestId); 
            
            //
            const testUser = {id:1};
            req.user = testUser;

            const context = {
                requestId, container,
                user: req.user,  
            }; 
            container.set("context", context); 

            return context;
        },
        plugins: [
            {
                requestDidStart: async (requestContext: GraphQLRequestContext<BaseContext>) => ({
                    async willSendResponse(requestContextIn: GraphQLRequestContext<BaseContext>) {
                        Container.reset(requestContextIn.context.requestId);
                    },
                }),
            },
        ],

    });


    const options: jwt.Options = {
        secret: "TypeGraphQL",
        credentialsRequired: false,
        algorithms: []
    };

    app.use(
        path,
        jwt(options),
    );

    await server.start();

    server.applyMiddleware({ app, path });

    app.listen({ port: PORT }, () =>
        console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`),
    );
}

main();

