import { AuthChecker } from 'type-graphql'

// export function customAuthChecker<ContextType>  ( { root, args, context, info },  roles, )  : AuthChecker<ContextType> {
//     // here we can read the user from context
//     // and check his permission in the db against the `roles` argument
//     // that comes from the `@Authorized` decorator, eg. ["ADMIN", "MODERATOR"]

//     return true; // or false if access is denied
// };

export const customAuthChecker: AuthChecker = (
    { root, args, context, info },
    roles,
  ) => {
    // here we can read the user from context
    // and check his permission in the db against the `roles` argument
    // that comes from the `@Authorized` decorator, eg. ["ADMIN", "MODERATOR"]
  
    return true; // or false if access is denied
  };