import { Field, InputType } from "type-graphql";

@InputType()
export class AuthorInput {

    @Field({ nullable: true })
    id: number;

    @Field({ nullable: true })
    name: string;

}