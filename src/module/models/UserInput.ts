import { IsEmail, Length } from "class-validator";
import { Field, InputType } from "type-graphql";

@InputType()
export class UserInput {

    @Field()
    firstName: string;

    @Field()
    lastName: string;

    @Length(1, 37)
    @IsEmail()
    @Field()
    email: string;

    @Field()
    @Length(7, 31)
    password: string;
}