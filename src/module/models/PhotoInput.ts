import { Field, InputType } from "type-graphql";
import { Author } from "../../entity";
import { AuthorInput } from "./AuthorInput";
import { PhotoMetadataInput } from "./PhotoMetadataInput";

@InputType()
export class PhotoInput {

    @Field({ nullable: true })
    id: number;

    @Field({ nullable: true })
    name: string;

    @Field({ nullable: true })
    description: string;

    @Field({ nullable: true })
    filename: string;

    @Field({ nullable: true })
    views: number;

    @Field({ nullable: true })
    isPublished: boolean;

    @Field({ simple: true, nullable: true })
    metadata: PhotoMetadataInput;

    @Field({ simple: false, nullable: true })
    author: AuthorInput;
}