import { Field, InputType } from "type-graphql";

@InputType()
export class PhotoMetadataInput {

    @Field({ nullable: true })
    id: number;

    @Field({ nullable: true })
    height: number;

    @Field({ nullable: true })
    width: number;

    @Field({ nullable: true })
    orientation: string;

    @Field({ nullable: true })
    compressed: boolean;

    @Field({ nullable: true })
    comment: string;
}