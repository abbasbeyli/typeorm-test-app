import { Arg, Args, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { Inject, Service } from "typedi";

import { User } from "../../entity";
import { UserService } from "../../services/UserService";
import { ListArgs } from "../models/ListArgs";
import { UserInput } from "../models/UserInput";

@Service()
@Resolver(of => User)
export class UserResolver {

  @Inject()
  private userService: UserService;

  @Query(() => User)
  async currentUser(
    @Ctx("user") user: User
  ) {
    return await this.getUserById(user?.id);
  }

  @Query(() => User)
  async user(
    @Arg("id", { nullable: false }) id: number
  ) {
    return await this.getUserById(id);
  }

  @Query(() => [User])
  async users(@Args() { skip, take }: ListArgs) {
    const users = await this.userService.findAllAsync({ skip, take });

    return users.map(x => ({ ...x, password: undefined }));
  }

  @Mutation(() => User)
  async registerUser(
    @Arg("newUserInput") newUserInput: UserInput,
    @Ctx("user") user: User,
  ): Promise<User> {
    let result: User;

    try {
      const data = await this.userService.validateAndMapForSave(newUserInput, user);
      result = await this.userService.addAsync(data);
    } catch (error) {
      console.log('addUser error', error)
      result = new User();
    }

    return result;
  }

  @Mutation(() => Boolean)
  async removeUser(@Arg("id") id: number) {
    try {
      await this.userService.removeByIdAsync(id);
      return true;
    } catch (error) {
      console.log('removeUser error', error)
      return false;
    }
  }

  private async getUserById(id: number) {
    if (!id) {
      throw new Error("id_must_be_provided");
    }
    let user: User;
    try {

      user = (await this.userService.findByIdAsync(id)) || new User();

      console.log('user', user);

    } catch (error) {
      user = new User();
      console.log('user error', error);
    }

    return user;
  }
}