import { Arg, Args, Authorized, Ctx, Info, Mutation, Query, Resolver } from "type-graphql";
import { Inject, Service } from "typedi";

import { Photo, PhotoMetadata, User } from "../../entity";
import { PhotoService } from "../../services/PhotoService";
import { PhotoInput } from "../models/PhotoInput";
import { ListArgs } from "../models/ListArgs";
import { GraphQLResolveInfo } from "graphql";

@Service()
@Resolver(of => Photo)
export class PhotoResolver {

  @Inject()
  private photoService: PhotoService;

  @Query(returns => Photo)
  async photo(@Arg("id", { nullable: false }) id: number) {
    if (!id) {
      throw new Error("id_must_be_provided")
    }
    let photo: Photo;
    try {

      photo = (await this.photoService.findByIdAsync(id, { relations: ["metadata"] })) || new Photo();

      console.log('photo', photo)

    } catch (error) {
      photo = new Photo();
      console.log('photo error', error)
    }

    return photo;
  }

  @Query(returns => [Photo])
  photos(@Args() { skip, take }: ListArgs, @Info() info: GraphQLResolveInfo) {
    console.log(info.fieldNodes[0].selectionSet?.selections);

    const relations: string[] | undefined = info.fieldNodes[0].selectionSet?.selections
      .filter((x: any) => x?.selectionSet && x?.name?.value)
      .map((x: any) => x.name.value as string);

    return this.photoService.findAllAsync({ skip, take, relations });
  }

  @Mutation(returns => Photo)
  async addPhoto(
    @Arg("newPhotoInput") newPhotoInput: PhotoInput,
    @Ctx("user") user: User,
  ): Promise<Photo> {
    let result: Photo;

    try {
      const data = await this.photoService.validateAndMapForSave(newPhotoInput, user);
      result = await this.photoService.addAsync(data);
    } catch (error) {
      console.log('addPhoto error', error)
      result = new Photo();
    }

    return result;
  }

  @Mutation(returns => Boolean)
  async removePhoto(@Arg("id") id: number) {
    try {
      await this.photoService.removeByIdAsync(id);
      return true;
    } catch (error) {
      console.log('removePhoto error', error)
      return false;
    }
  }
}